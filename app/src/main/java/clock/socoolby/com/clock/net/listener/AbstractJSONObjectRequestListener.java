package clock.socoolby.com.clock.net.listener;

import org.json.JSONObject;

import clock.socoolby.com.clock.net.base.I_JsonObject;

public abstract class AbstractJSONObjectRequestListener<T extends I_JsonObject> implements RequestListener<JSONObject> {

    @Override
    public void onResponse(JSONObject response) {
        if (null==response||!isResponseSucceed(response)) {
            onRequestFailed(-1, "response check false.");
            return;
        }
    }

    public abstract T creatEntity();

    protected boolean isResponseSucceed(JSONObject response){
        return true;
    }
}
