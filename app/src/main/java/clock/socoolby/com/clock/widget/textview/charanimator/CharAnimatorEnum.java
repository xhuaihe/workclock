package clock.socoolby.com.clock.widget.textview.charanimator;

public enum CharAnimatorEnum{
    NOSETUP(0),UP2DOWN(1),DOWN2UP(2),Marquee3D_Up(3),Marquee3D_Down(4),TabDigit(5);

    int styleCode;

    CharAnimatorEnum(int styleCode) {
        this.styleCode = styleCode;
    }

    public static CharAnimatorEnum valueOf(int code){
        for (CharAnimatorEnum  styleEnum: values()) {
                if(styleEnum.styleCode == code){
                       return  styleEnum;
                 }
              }
        return NOSETUP;
    }

    public int getStyleCode() {
        return styleCode;
    }
}
