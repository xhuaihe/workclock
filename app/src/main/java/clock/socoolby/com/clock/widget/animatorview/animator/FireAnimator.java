package clock.socoolby.com.clock.widget.animatorview.animator;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.List;
import clock.socoolby.com.clock.widget.animatorview.AbstractCacheDifferenceAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

//参考: https://www.jianshu.com/p/0d4ab16a6b7b
public class FireAnimator extends AbstractCacheDifferenceAnimator<FireAnimator.ParticleSystem, FireAnimator.Particle> {

    public static final String NAME="Fire";

    public FireAnimator() {
        super(1);
    }

    int high=0;
    int fireWidth=0;
    int particlesCount;
    @Override
    public ParticleSystem createNewEntry() {
        high=rand.nextInt(height);
        if(high<=height/3)
            high=high+height/2;
        randomColorIfAble();
        fireWidth=rand.nextInt(width/2);
        particlesCount=rand.nextInt(300);
        particlesCount=particlesCount>150?particlesCount:particlesCount+150;
        return new ParticleSystem(width/2,height,high,fireWidth,particlesCount,rand.nextInt(30),color);
    }


    public class ParticleSystem implements I_AnimatorEntry {
        private List<Particle> particles = new ArrayList<>();
        private int fire_width = 20;
        private int particle_width=20;
        private int x,y,high;
        private int color;

        public ParticleSystem(int x,int y,int high,int fire_width,int particlesCount,int particle_width,int color) {
            this.x=x;
            this.y=y;
            this.high=high;
            this.fire_width =fire_width;
            this.particle_width=particle_width;
            if(particle_width<10)
                this.particle_width=10;
            this.color=color;
            for (int i = 0; i < particlesCount; i++) {
                particles.add(newParticle());
            }
        }



        /**
         * 粒子集合更新方法
         *
         * @param wind wind是风，volume 是风加速度 0时代表无风，粒子不偏移
         */
        public void update(WIND wind,float volume) {
            for (int i = 0; i < particles.size(); i++) {
                Particle particle = particles.get(i);

                if (particle.xv > 0) {
                    //当初始加速度为正，就一直为正
                    particle.xv += 0.01f + volume;
                } else {
                    //当初始加速度为负，就一直为正
                    particle.xv += -0.01f + volume;
                }
                particle.yv = particle.yv + 0.1f;
                switch (wind){
                    case LEFT_TO_RIGHT:
                        break;
                    case RIGHT_TO_LEFT:
                        break;
                }

                particle.setX(particle.getX() + particle.xv);
                particle.setY(particle.getY() - particle.yv);
                //跟据上升高度 计算透明度
                particle.alpha = 255-(int) ((y-particle.y)*255/high);
            }
            List<Particle> list = new ArrayList<>();
            list.addAll(particles);
            for (Particle particle : list) {
                if (particle.getY() < y-high) {
                    moveToTrashCache(particle);
                    particles.remove(particle);
                }
            }
            for (int i = 0; i < 5; i++) {
                particles.add(newParticle());
            }
        }


        public Particle newParticle(){
            Particle particle =revectForTrashCache();
            if(particle==null)
                particle = new Particle();
            if(rand.nextBoolean())
                particle.x=x - rand.nextInt(fire_width /2);
            else
                particle.x=x + rand.nextInt(fire_width /2);
            particle.y=y;
            particle.xv = 0.5f - 1 * rand.nextFloat();
            particle.yv=1;
            particle.alpha=255;
            return particle;
        }

        @Override
        public void move(int maxWidth, int maxHight) {
            update(WIND.NULL,0);
        }

        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {
            mPaint.setColor(color);
            for (int i = 0; i < particles.size(); i++) {
                Particle particle = particles.get(i);
                mPaint.setAlpha(particle.alpha);
                canvas.drawRect(particle.getX(), particle.getY(), particle.getX() + particle_width, particle.getY() + particle_width, mPaint);          }
        }

        @Override
        public void setAnimatorEntryColor(int color) {
            this.color=color;
        }
    }

    //用于两边向中间靠拢
    public enum WIND{
        LEFT_TO_RIGHT,NULL,RIGHT_TO_LEFT,
        TORNADO;//龙卷风
    }

    public class Particle {
        //火焰粒子坐标
        private float x;
        private float y;
        public float xv = 1;//x轴加速度
        public float yv = 1;//y轴加速度
        public int alpha;

        public Particle(){
        }

        public Particle(float x, float y) {
            this.x = x;
            this.y = y;
        }

        public float getX() {
            return x;
        }

        public void setX(float x) {
            this.x = x;
        }

        public float getY() {
            return y;
        }

        public void setY(float y) {
            this.y = y;
        }
    }
}
