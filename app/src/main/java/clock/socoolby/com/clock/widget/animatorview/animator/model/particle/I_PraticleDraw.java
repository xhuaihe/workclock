package clock.socoolby.com.clock.widget.animatorview.animator.model.particle;

import android.graphics.Canvas;
import android.graphics.Paint;

public interface I_PraticleDraw {
     void draw(Canvas canvas, Particle particle, Paint paint);
}
