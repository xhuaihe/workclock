package clock.socoolby.com.clock.fragment.handup;

import clock.socoolby.com.clock.state.ClockStateMachine;

public abstract class AbstractHandUpSelectFragment extends AbstractHandUpFragment {


    public AbstractHandUpSelectFragment( int view_id) {
        super( view_id);
    }

    protected void selected(){
        this.endHandUp(true);
    }
}
