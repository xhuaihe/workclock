package clock.socoolby.com.clock.fragment.digit;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import clock.socoolby.com.clock.ClockApplication;
import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.dao.base.TimeFontStyle;
import clock.socoolby.com.clock.event.EventListenerHandle;
import clock.socoolby.com.clock.event.EventManger;
import clock.socoolby.com.clock.state.ClockStateMachine;
import clock.socoolby.com.clock.utils.FontUtils;
import clock.socoolby.com.clock.viewmodel.DigitViewModel;
import clock.socoolby.com.clock.viewmodel.GlobalViewModel;
import clock.socoolby.com.clock.viewmodel.ViewModelFactory;
import clock.socoolby.com.clock.widget.textview.DigitTextView;
import clock.socoolby.com.clock.widget.textview.ShadowTypeEnum;
import clock.socoolby.com.clock.widget.textview.charanimator.CharAnimatorEnum;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class DigitClockFragment extends Fragment {
    public static final String TAG=DigitClockFragment.class.getName();
    public ClockStateMachine clockStateMachine;

    public DigitClockFragment(){

    }

    DigitViewModel digitViewModel;
    GlobalViewModel globalViewModel;

    private DigitTextView tv_time;

    EventListenerHandle heartbeatListenerHandle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        digitViewModel= ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(DigitViewModel.class);
        globalViewModel=ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(GlobalViewModel.class);
        clockStateMachine=globalViewModel.getClockStateMachine();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_digit_clock, container, false);
        tv_time=view.findViewById(R.id.tv_time);
        bindViewModel();
        //进行
        EventManger.addCountingStateChangedListener(this,event->{
                if(event.getValue()){
                    heartbeatListenerHandle=EventManger.addHeartbeatListener(event1 -> tv_time.setVisibility(tv_time.getVisibility()==View.VISIBLE?View.GONE:View.VISIBLE));
                }else{
                    tv_time.setVisibility(View.VISIBLE);
                    if(heartbeatListenerHandle!=null) {
                        EventManger.removeClockEventListener(heartbeatListenerHandle);
                        heartbeatListenerHandle=null;
                    }
                }
            });
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        if(heartbeatListenerHandle!=null)
            EventManger.removeClockEventListener(heartbeatListenerHandle);
    }

    private void bindViewModel(){
        digitViewModel.getTimeFontStyle().observe(this, new Observer<TimeFontStyle>() {
            @Override
            public void onChanged(TimeFontStyle timeFontStyle) {
                reflushFontStyle(timeFontStyle);
            }
        });

        digitViewModel.getTimeFontStyleSize().observe(this, new Observer<Integer>() {
             @Override
             public void onChanged(Integer integer) {
                 setFontSize(integer);
             }
         });

        digitViewModel.getBaseLineDown().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                tv_time.setBaseLineDown(integer);
            }
        });

        digitViewModel.getBaseLineX().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                tv_time.setBaseLineY(integer);
            }
        });

        digitViewModel.getLinearGradientAble().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                tv_time.setLinearGradientAble(aBoolean);
            }
        });

        digitViewModel.getTimeLinearGradientColorsArray().observe(this, new Observer<Integer[]>() {
            @Override
            public void onChanged(Integer[] integers) {
               if(integers==null) {
                   tv_time.setLinearGradientColors(null);
                   return;
               }
               int[] toInts=new int[integers.length];
               for(int i=0;i<integers.length;i++)
                   toInts[i]=integers[i];
                tv_time.setLinearGradient(toInts);
            }
        });

        digitViewModel.getReflectedAble().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                tv_time.setReflectedAble(aBoolean);
            }
        });

        digitViewModel.getShadowType().observe(this, new Observer<ShadowTypeEnum>() {
            @Override
            public void onChanged(ShadowTypeEnum shadowTypeEnum) {
                tv_time.setShadowType(shadowTypeEnum);
            }
        });

        digitViewModel.getTimeText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                tv_time.setText(s);
            }
        });

        digitViewModel.getTimeCharAnimatorType().observe(this, new Observer<CharAnimatorEnum>() {
            @Override
            public void onChanged(CharAnimatorEnum charAnimatorEnum) {
                tv_time.setCurrentCharAnimatorType(charAnimatorEnum);
            }
        });

        globalViewModel.getForegroundColor().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                tv_time.setTextColor(integer);
            }
        });

        digitViewModel.getSecondSubscript().observe(this,isSubscript->tv_time.setSecondSubscript(isSubscript));

        digitViewModel.getCharBackgroundBorder().observe(this,able->tv_time.setCharBackgroundBorder(able));

        digitViewModel.getCharBackgroundBorderColor().observe(this,color->tv_time.setCharBackgroundBorderColor(color));

        //globalViewModel.getBackgroundColor().observe(this,color->tv_time.setBackgroundBorderDividerColor(color));

        digitViewModel.getCharBackgroundBorderDividerColor().observe(this,color->tv_time.setBackgroundBorderDividerColor(color));

        digitViewModel.getCharBackgroundBorderDividerWidth().observe(this,width->tv_time.setBackgroundBorderDividerWidth(width));

        digitViewModel.getSubscriptFontScale().observe(this,scale->tv_time.setFontScale(scale));

        digitViewModel.getTimeTextPadding().observe(this,padding->tv_time.setPaddingScale(padding));

        digitViewModel.getCharBackgroundBorderWithDoubble().observe(this,doubble->tv_time.setCharBackgroundBorderWithDoubble(doubble));

    }


    protected void reflushFontStyle(TimeFontStyle fontStyle){
        FontUtils.getInstance().replaceFontFromAsset(tv_time,"fonts/"+fontStyle.name+".ttf");
    }

    private void setFontSize(int fontSize){
        tv_time.setTextSize(fontSize);
    }
}
