package clock.socoolby.com.clock.widget.animatorview;

import android.graphics.Point;


public interface I_AngleTrajectory extends I_Trajectory {

    //0-360;
    Point getAnagePoint(float anage);

    //各顶点座标
    Point[] getVertexs();
}
