package clock.socoolby.com.clock.widget.animatorview.animator;


import clock.socoolby.com.clock.R;

public class StarFallAnimator extends DrawableArrayFallAnimator {

    public static final String NAME="StarFall";

    //小星星图片的资源文件
     static int[] picRes = {R.drawable.ic_star
            ,R.drawable.ic_star1
            ,R.drawable.ic_star2
            ,R.drawable.ic_star3
            ,R.drawable.ic_star4
            ,R.drawable.ic_star5
            ,R.drawable.ic_star6
            ,R.drawable.ic_star7
    };

    public StarFallAnimator() {
        super(picRes,10);
    }
}
