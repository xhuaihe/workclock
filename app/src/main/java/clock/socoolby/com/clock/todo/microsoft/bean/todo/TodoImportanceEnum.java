package clock.socoolby.com.clock.todo.microsoft.bean.todo;

public enum TodoImportanceEnum {
    LOW("low"),NORMAL("normal"),HIGH("high");

    String value;

    TodoImportanceEnum(String value) {
        this.value=value;
    }

    public String getValue() {
        return value;
    }


    public static TodoImportanceEnum build(String value){
        switch (value){
            case "normal":
                return NORMAL;
            case "low":
                return LOW;
            case "high":

        }
        return HIGH;
    }
}
