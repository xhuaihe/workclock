package clock.socoolby.com.clock.todo.microsoft.listener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import clock.socoolby.com.clock.net.base.I_JsonObject;
import clock.socoolby.com.clock.net.base.I_ResponseState;
import clock.socoolby.com.clock.net.listener.AbstractEntityListRequestListener;
import clock.socoolby.com.clock.net.listener.StateAbleRequestListener;
import clock.socoolby.com.clock.todo.microsoft.bean.MircsoftReponseState;

public abstract class AbstractMicrosoftEntityListRequestListener<T extends I_JsonObject> extends AbstractEntityListRequestListener<T, I_ResponseState> {
    public static final String LIST_STR="value";

    public AbstractMicrosoftEntityListRequestListener(StateAbleRequestListener<List<T>, I_ResponseState> warpListener) {
        super(LIST_STR, warpListener);
    }

    @Override
    public I_ResponseState createState(JSONObject response) throws JSONException {
        MircsoftReponseState reponseState=new MircsoftReponseState();
        reponseState.fromJson(response);
        return reponseState;
    }
}
