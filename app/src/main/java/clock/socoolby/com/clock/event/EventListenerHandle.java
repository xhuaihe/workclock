package clock.socoolby.com.clock.event;

import androidx.lifecycle.Observer;

public class EventListenerHandle<T>{
    String eventType;
    Class<T> valueClass;
    Observer<T> handle;

    public EventListenerHandle(String eventType, Class<T> valueClass, Observer<T> handle) {
        this.eventType = eventType;
        this.valueClass = valueClass;
        this.handle = handle;
    }

    public String getEventType() {
        return eventType;
    }

    public Class<T> getValueClass() {
        return valueClass;
    }

    public Observer<T> getHandle() {
        return handle;
    }
}
