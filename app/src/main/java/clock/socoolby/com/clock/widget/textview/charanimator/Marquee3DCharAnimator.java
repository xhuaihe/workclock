package clock.socoolby.com.clock.widget.textview.charanimator;

import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

//参考：https://github.com/xiangcman/Marquee3DView
public class Marquee3DCharAnimator extends AbstractCharAnimator {

    //从下到上进行旋转滚动
    public static final int D2U = 1;
    //从上到下进行旋转滚动
    public static final int U2D = 2;

    //负责围绕那个轴进行旋转
    private Camera camera;
    //负责控制旋转点
    private Matrix matrix;

    private float changeRotate;

    private float translateY;

    private int direction;

    public Marquee3DCharAnimator(String preString, String currentString,int direction) {
        super(preString, currentString);
        this.direction=direction;
        initialize();
    }

    public Marquee3DCharAnimator(String preString, String currentString,int direction, float runSpeed) {
        super(preString, currentString, runSpeed);
        this.direction=direction;
        initialize();
    }

    private void initialize() {
        camera = new Camera();
        matrix = new Matrix();
    }

    @Override
    public void move() {
        super.move();
        changeRotate = 90*charAnimatorPercent;
    }

    @Override
    public void drawCharPre(Canvas canvas, String strToDraw, float startX, float startY, Paint mTextPaint, float percent) {
        //从0到height的一个过程
        translateY = startY *charAnimatorPercent;

        int alpha = (int) (255 - percent * 255);
        mTextPaint.setAlpha(alpha);

        float width=startX;
        float height=startY;

        canvas.save();
        camera.save();
        if (direction == D2U) {
            //当前的item是往里面转动的，因此角度是增大的
            camera.rotateX(changeRotate);//0到90度的过程
        } else {
            camera.rotateX(-changeRotate);
        }
        camera.getMatrix(matrix);
        camera.restore();
        if (direction == D2U) {
            matrix.preTranslate(-width / 2, -height);
            matrix.postTranslate(width / 2, height - translateY);//最后跑到0的位置了
        } else {
            matrix.preTranslate(-width / 2, 0);
            matrix.postTranslate(width / 2, translateY);
        }
        canvas.setMatrix(matrix);
        canvas.drawText(strToDraw, startX, startY, mTextPaint);
        canvas.restore();
    }

    @Override
    public void drawCharCurrent(Canvas canvas, String strToDraw, float startX, float startY, Paint mTextPaint, float percent) {

        translateY = startY *charAnimatorPercent;

        float width=startX;
        float height=startY;

        int alpha = (int) (percent * 255);
        mTextPaint.setAlpha(alpha);

        canvas.save();
        camera.save();
        if (direction == D2U) {
            //从-90度到0度的过程
            camera.rotateX(-90 + changeRotate);
        } else {
            //从上到下是90度到0度的过程
            camera.rotateX(90 - changeRotate);
        }
        camera.getMatrix(matrix);
        camera.restore();

        if (direction == D2U) {
            matrix.preTranslate(-width / 2, 0);
            matrix.postTranslate(width / 2, height + (-translateY));
        } else {
            matrix.preTranslate(-width / 2, -height);
            matrix.postTranslate(width / 2, translateY);
        }

        canvas.setMatrix(matrix);

        canvas.drawText(strToDraw, startX, startY, mTextPaint);
        canvas.restore();
    }
}
