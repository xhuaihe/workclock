package clock.socoolby.com.clock.net.listener;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.net.base.I_JsonObject;

public abstract class AbstractEntityRequestListener<T extends I_JsonObject> extends AbstractJSONObjectRequestListener<T> {
    RequestListener<T> warpListener;

    public AbstractEntityRequestListener(RequestListener<T> warpListener) {
        this.warpListener = warpListener;
    }

    @Override
    public void onResponse(JSONObject response) {
        super.onResponse(response);
        T entity= creatEntity();
        try {
            entity.fromJson(response);
        } catch (JSONException e) {
            e.printStackTrace();
            entity=null;
        }
        warpListener.onResponse(entity);
    }


    @Override
    public void onRequestFailed(int error, String errorMessage) {
        warpListener.onRequestFailed( error,  errorMessage);
    }

}
