package clock.socoolby.com.clock.net.listener;

import clock.socoolby.com.clock.net.base.I_ResponseState;

public interface StateAbleRequestListener<T,Q extends I_ResponseState> {
    void onResponse(T response,Q state);

    void onRequestFailed(int error, String errorMessage);
}
