package clock.socoolby.com.clock.fragment.theme;

import clock.socoolby.com.clock.R;

public class ThemeUIDefaultFragment extends AbstractThemeUIFragment {

    public static final String THEME_NAME="default";

    public ThemeUIDefaultFragment() {
        super(R.layout.theme_default);
    }

    @Override
    void changeThemeTypeCheck() {
        themeUIViewModel.setThemeName(ThemeUISampleFragment.THEME_NAME);
    }
}
