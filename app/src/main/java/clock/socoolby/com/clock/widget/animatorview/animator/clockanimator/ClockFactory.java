package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

public class ClockFactory {

    public static AbstractClock build(String type){
        AbstractClock clock=null;
        switch (type){
            case CircleClock.TYPE_CIRCLE:
                clock=new CircleClock();
                break;
            case CircleTwoClock.TYPE_CIRCLE_TWO:
                clock=new CircleTwoClock();
                break;
            case HelixClock.TYPE_HELIX:
                clock=new HelixClock();
                break;
            case HexagonalClock.TYPE_HEXAGONAL:
                clock=new HexagonalClock();
                break;
            case OctagonalClock.TYPE_OCTAGONAL:
                clock=new OctagonalClock();
                break;
            case OvalClock.TYPE_OVAL:
                clock=new OvalClock();
                break;
            case SquareClock.TYPE_SQUARE:
                clock=new SquareClock();
                break;
            default:
                clock=new CircleClock();
        }
        return clock;
    }

    public static final String[] CLOCKS_ARRAY=new String[]{
            CircleClock.TYPE_CIRCLE,
            CircleTwoClock.TYPE_CIRCLE_TWO,
            HelixClock.TYPE_HELIX,
            HexagonalClock.TYPE_HEXAGONAL,
            OctagonalClock.TYPE_OCTAGONAL,
            OvalClock.TYPE_OVAL,
            SquareClock.TYPE_SQUARE
    };

    public static String nextSimulateClockName(String currentName){
        int order=0;
        for(int i=0;i<CLOCKS_ARRAY.length;i++)
            if(CLOCKS_ARRAY[i].equalsIgnoreCase(currentName))
                order=i;
        order++;
        if(order==CLOCKS_ARRAY.length)
            order=0;
        return CLOCKS_ARRAY[order];

    }
}
