package clock.socoolby.com.clock.todo.microsoft.bean.attachment;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * contentBytes	Edm	文件的 Base64 编码内容。
 * contentId	String	获取 Exchange 存储中的附件 ID。
 * contentLocation	String	请勿使用此属性，因为它不受支持。
 *
 */

public class FileAttachment extends Attachment {
    Byte[] contentBytes;
    String contentId;
    String contentLocation;


    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        super.fromJson(jsonObject);
    }
}
