package clock.socoolby.com.clock.todo;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.Constants;
import clock.socoolby.com.clock.model.AbstractPerferenceModel;
import clock.socoolby.com.clock.todo.config.TodoSyncConfig;

public class TodoSyncPerferenceModel extends AbstractPerferenceModel {

    TodoSyncConfig todoSyncConfig=new TodoSyncConfig();

    @Override
    protected String getConfigFileName() {
        return Constants.TODO_SYNC_PERFERENCE_FILE;
    }

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        todoSyncConfig.fromJson(jsonObject);
    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {
        todoSyncConfig.toJson(jsonObject);
    }

    public TodoSyncConfig getTodoSyncConfig() {
        return todoSyncConfig;
    }

    public void setTodoSyncConfig(TodoSyncConfig todoSyncConfig) {
        this.todoSyncConfig = todoSyncConfig;
    }
}
