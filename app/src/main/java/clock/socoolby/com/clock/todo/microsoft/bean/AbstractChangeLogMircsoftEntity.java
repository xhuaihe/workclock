package clock.socoolby.com.clock.todo.microsoft.bean;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class AbstractChangeLogMircsoftEntity extends AbstractMircsoftEntity {
    public static final String CHANGEKEY="changeKey";
    protected String changekey;

    public  void fromJson(JSONObject jsonObject) throws JSONException {
        super.fromJson(jsonObject);
        changekey=jsonObject.getString(CHANGEKEY);
    }


    public void setChangekey(String changekey) {
        this.changekey = changekey;
    }

    public String getChangekey() {
        return changekey;
    }
}
