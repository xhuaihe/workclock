package clock.socoolby.com.clock.event;

public interface ClockEventListener<T> {
   void onEvent(ClockEvent<T> event);
}
