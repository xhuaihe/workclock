package clock.socoolby.com.clock.model;

import org.json.JSONException;
import org.json.JSONObject;

import static clock.socoolby.com.clock.model.SharePerferenceModel.DEFAULT_COLOR;

public class SimulatePerferenceModel {

    protected  final static String KEY_SIMULATE_CLOCK_TYPE_NAME="key_simulate_clock_type_name";
    protected String simulateClockTypeName="default";

    protected  final static String KEY_SIMULATE_CLOCK_POINTER_TYPE_NAME="key_simulate_clock_pointer_type_name";
    protected String simulateClockPointerTypeName="default";

    protected  final static String KEY_SIMULATE_CLOCK_COLOR_SCALE="key_simulate_clock_color_scale";
    protected Integer simulateClockColorScale= DEFAULT_COLOR;

    protected  final static String KEY_SIMULATE_CLOCK_COLOR_SCALE_PARTICULARLY="key_simulate_clock_color_scale_particularly";
    protected Integer simulateClockColorScaleParticularly=DEFAULT_COLOR;

    protected  final static String KEY_SIMULATE_CLOCK_COLOR_TEXT="key_simulate_clock_color_text";
    protected Integer  simulateClockColorText=DEFAULT_COLOR;

    protected  final static String KEY_SIMULATE_CLOCK_COLOR_POINTER="key_simulate_clock_color_pointer";
    protected Integer  simulateClockColorPointer=DEFAULT_COLOR;

    protected  final static String KEY_SIMULATE_CLOCK_COLOR_POINTER_SECOND="key_simulate_clock_color_pointer_second";
    protected Integer  simulateClockColorPointerSecond=DEFAULT_COLOR;

    protected  final static String KEY_SIMULATE_CLOCK_COLOR_OUTLINE="key_simulate_clock_color_outLine";
    protected Integer  simulateClockColorOutLine=DEFAULT_COLOR;

    protected  final static String KEY_SIMULATE_CLOCK_TEXT_SHOW_TYPE="key_simulate_clock_text_show_type";
    protected  Integer  simulateClockTextShowType=0;

    public void fromJsonString(JSONObject jsonObject) {
        simulateClockTypeName=jsonObject.optString(KEY_SIMULATE_CLOCK_TYPE_NAME,"default");
        simulateClockPointerTypeName=jsonObject.optString(KEY_SIMULATE_CLOCK_POINTER_TYPE_NAME,"default");
        simulateClockColorScale=jsonObject.optInt(KEY_SIMULATE_CLOCK_COLOR_SCALE, DEFAULT_COLOR);
        simulateClockColorScaleParticularly=jsonObject.optInt(KEY_SIMULATE_CLOCK_COLOR_SCALE_PARTICULARLY,DEFAULT_COLOR);
        simulateClockColorText=jsonObject.optInt(KEY_SIMULATE_CLOCK_COLOR_TEXT,DEFAULT_COLOR);
        simulateClockColorOutLine=jsonObject.optInt(KEY_SIMULATE_CLOCK_COLOR_OUTLINE,DEFAULT_COLOR);
        simulateClockTextShowType=jsonObject.optInt(KEY_SIMULATE_CLOCK_TEXT_SHOW_TYPE,0);
        simulateClockColorPointer=jsonObject.optInt(KEY_SIMULATE_CLOCK_COLOR_POINTER,DEFAULT_COLOR);
        simulateClockColorPointerSecond=jsonObject.optInt(KEY_SIMULATE_CLOCK_COLOR_POINTER_SECOND,DEFAULT_COLOR);
    }


    public void toJsonString(JSONObject jsonObject) throws JSONException {
        jsonObject.put(KEY_SIMULATE_CLOCK_TYPE_NAME,simulateClockTypeName);
        jsonObject.put(KEY_SIMULATE_CLOCK_POINTER_TYPE_NAME,simulateClockPointerTypeName);

        jsonObject.put(KEY_SIMULATE_CLOCK_COLOR_SCALE,simulateClockColorScale);
        jsonObject.put(KEY_SIMULATE_CLOCK_COLOR_SCALE_PARTICULARLY,simulateClockColorScaleParticularly);
        jsonObject.put(KEY_SIMULATE_CLOCK_COLOR_TEXT,simulateClockColorText);
        jsonObject.put(KEY_SIMULATE_CLOCK_COLOR_OUTLINE,simulateClockColorOutLine);
        jsonObject.put(KEY_SIMULATE_CLOCK_TEXT_SHOW_TYPE,simulateClockTextShowType);
        jsonObject.put(KEY_SIMULATE_CLOCK_COLOR_POINTER,simulateClockColorPointer);
        jsonObject.put(KEY_SIMULATE_CLOCK_COLOR_POINTER_SECOND,simulateClockColorPointerSecond);
    }

    public String getSimulateClockTypeName() {
        return simulateClockTypeName;
    }

    public void setSimulateClockTypeName(String simulateClockTypeName) {
        this.simulateClockTypeName = simulateClockTypeName;
    }

    public String getSimulateClockPointerTypeName() {
        return simulateClockPointerTypeName;
    }

    public void setSimulateClockPointerTypeName(String simulateClockPointerTypeName) {
        this.simulateClockPointerTypeName = simulateClockPointerTypeName;

    }


    public Integer getSimulateClockColorScale() {
        return simulateClockColorScale;
    }

    public void setSimulateClockColorScale(Integer simulateClockColorScale) {
        this.simulateClockColorScale = simulateClockColorScale;

    }

    public Integer getSimulateClockColorScaleParticularly() {
        return simulateClockColorScaleParticularly;
    }

    public void setSimulateClockColorScaleParticularly(Integer simulateClockColorScaleParticularly) {
        this.simulateClockColorScaleParticularly = simulateClockColorScaleParticularly;

    }

    public Integer getSimulateClockColorText() {
        return simulateClockColorText;
    }

    public void setSimulateClockColorText(Integer simulateClockColorText) {
        this.simulateClockColorText = simulateClockColorText;

    }

    public Integer getSimulateClockColorOutLine() {
        return simulateClockColorOutLine;
    }

    public void setSimulateClockColorOutLine(Integer simulateClockColorOutLine) {
        this.simulateClockColorOutLine = simulateClockColorOutLine;

    }

    public Integer getSimulateClockTextShowType() {
        return simulateClockTextShowType;
    }

    public void setSimulateClockTextShowType(Integer simulateClockTextShowType) {
        this.simulateClockTextShowType = simulateClockTextShowType;

    }

    public Integer getSimulateClockColorPointer() {
        return simulateClockColorPointer;
    }

    public void setSimulateClockColorPointer(Integer simulateClockColorPointer) {
        this.simulateClockColorPointer = simulateClockColorPointer;

    }

    public Integer getSimulateClockColorPointerSecond() {
        return simulateClockColorPointerSecond;
    }

    public void setSimulateClockColorPointerSecond(Integer simulateClockColorPointerSecond) {
        this.simulateClockColorPointerSecond = simulateClockColorPointerSecond;

    }
}
