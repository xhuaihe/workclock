package clock.socoolby.com.clock.model;

import android.graphics.Color;

import org.json.JSONException;
import org.json.JSONObject;

public class DigitPerferenceModel {
    protected final static String KEY_FONT_NAME = "key_font_name";
    protected String fontName="default";

    protected final static String KEY_FONT_STYLE_SIZE = "key_font_style_size";
    protected Integer fontStyleSize =100;


    protected  final static String KEY_IS_LINEARGRADIENT_ABLE="key_time_text_is_linearGradient_able";
    protected boolean linearGradientAble = false;

    protected final static String KEY_FOREGROUND_TIME_COLORS_ARRAY="key_time_text_foreground_time_colors_array";
    protected Integer[] timeColorsArray=null;

    protected  final static String KEY_IS_REFLECTED_ABLE="key_time_text_is_reflectedAble_able";
    protected boolean reflectedAble = false;

    protected final static String KEY_TIME_TEXT_SHADOW_TYPE="key_time_text_shadowType";
    protected Integer shadowType=0;

    protected final static String KEY_TIME_TEXT_CHAR_ANIMATOR_TYPE="key_time_text_charAnimatorType";
    protected Integer timeTextCharAnimatorType=0;

    protected final static String KEY_IS_DISPLAY_SECOND = "key_is_display_second";
    protected boolean displaySecond = true;

    protected final static String KEY_IS_DISPLAY_SECOND_SUBSCRIPT = "key_is_display_second_subscript";
    protected boolean timeTextSecondSubscript = true;

    protected final static String KEY_IS_CHAR_BACKGROUND_BORDER = "key_is_char_background_border";
    protected boolean charBackgroundBorder = false;

    protected final static String KEY_IS_CHAR_BACKGROUND_BORDER_COLOR = "key_is_char_background_border_color";
    protected Integer charBackgroundBorderColor=Color.BLACK;

    protected final static String KEY_IS_CHAR_BACKGROUND_BORDER_DIVIDER_COLOR = "key_is_char_background_border_divider_color";
    protected Integer charBackgroundBorderDividerColor=Color.BLACK;

    protected final static String KEY_IS_CHAR_BACKGROUND_BORDER_DIVIDER_STROKE_WIDTH = "key_is_char_background_border_divider_stroke_width";
    protected Integer charBackgroundBorderDividerStrokeWidth=10;

    protected final static String KEY_TIME_TEXT_BASELINE_DOWN= "key_time_text_baseline_down";
    protected Integer baseLineDown=13;

    protected final static String KEY_TIME_TEXT_BASELINE_Y= "key_time_text_baseline_y";
    protected Integer baseLineX =0;

    protected final static String KEY_TIME_TEXT_SUBSCRIPT_FONT_SCALE= "key_time_text_subscript_font_scale";
    protected Integer subscriptFontScale=40;

    protected final static String KEY_TIME_TEXT_PADDING= "key_time_text_padding";
    protected Integer timeTextPadding=2;


    protected final static String KEY_IS_CHAR_BACKGROUND_BORDER_DUBBLE = "key_is_char_background_border_dubble";
    protected boolean charBackgroundBorderWithDoubble = false;


    public void fromJsonString(JSONObject jsonObject) throws JSONException {
        linearGradientAble=jsonObject.optBoolean(KEY_IS_LINEARGRADIENT_ABLE,false);
        timeColorsArray=stringToIntegerArray(jsonObject.optString(KEY_FOREGROUND_TIME_COLORS_ARRAY,null));
        reflectedAble=jsonObject.optBoolean(KEY_IS_REFLECTED_ABLE);
        shadowType=jsonObject.optInt(KEY_TIME_TEXT_SHADOW_TYPE,0);
        timeTextCharAnimatorType=jsonObject.optInt(KEY_TIME_TEXT_CHAR_ANIMATOR_TYPE,0);
        fontName=jsonObject.optString(KEY_FONT_NAME,"default");
        fontStyleSize =jsonObject.optInt(KEY_FONT_STYLE_SIZE,100);
        displaySecond = jsonObject.optBoolean(KEY_IS_DISPLAY_SECOND,false);
        timeTextSecondSubscript=jsonObject.optBoolean(KEY_IS_DISPLAY_SECOND_SUBSCRIPT,true);
        charBackgroundBorder=jsonObject.optBoolean(KEY_IS_CHAR_BACKGROUND_BORDER,false);
        charBackgroundBorderColor=jsonObject.optInt(KEY_IS_CHAR_BACKGROUND_BORDER_COLOR, Color.BLACK);
        baseLineDown=jsonObject.optInt(KEY_TIME_TEXT_BASELINE_DOWN,13);
        charBackgroundBorderDividerColor=jsonObject.optInt(KEY_IS_CHAR_BACKGROUND_BORDER_DIVIDER_COLOR, Color.BLACK);
        charBackgroundBorderDividerStrokeWidth=jsonObject.optInt(KEY_IS_CHAR_BACKGROUND_BORDER_DIVIDER_STROKE_WIDTH, 10);
        subscriptFontScale=jsonObject.optInt(KEY_TIME_TEXT_SUBSCRIPT_FONT_SCALE, 40);
        timeTextPadding=jsonObject.optInt(KEY_TIME_TEXT_PADDING, 2);
        charBackgroundBorderWithDoubble=jsonObject.optBoolean(KEY_IS_CHAR_BACKGROUND_BORDER_DUBBLE, false);
        baseLineX =jsonObject.optInt(KEY_TIME_TEXT_BASELINE_Y,0);
    }


    public void toJsonString(JSONObject jsonObject) throws JSONException {
        jsonObject.put(KEY_IS_LINEARGRADIENT_ABLE,linearGradientAble);
        jsonObject.put(KEY_FOREGROUND_TIME_COLORS_ARRAY,integerArrayToString(timeColorsArray));
        jsonObject.put(KEY_IS_REFLECTED_ABLE,reflectedAble);
        jsonObject.put(KEY_TIME_TEXT_SHADOW_TYPE,shadowType);
        jsonObject.put(KEY_TIME_TEXT_CHAR_ANIMATOR_TYPE,timeTextCharAnimatorType);
        jsonObject.put(KEY_FONT_NAME,fontName);
        jsonObject.put(KEY_FONT_STYLE_SIZE, fontStyleSize);
        jsonObject.put(KEY_IS_DISPLAY_SECOND, displaySecond);
        jsonObject.put(KEY_IS_DISPLAY_SECOND_SUBSCRIPT,timeTextSecondSubscript);
        jsonObject.put(KEY_IS_CHAR_BACKGROUND_BORDER,charBackgroundBorder);
        jsonObject.put(KEY_IS_CHAR_BACKGROUND_BORDER_COLOR,charBackgroundBorderColor);
        jsonObject.put(KEY_TIME_TEXT_BASELINE_DOWN,baseLineDown);
        jsonObject.put(KEY_IS_CHAR_BACKGROUND_BORDER_DIVIDER_COLOR,charBackgroundBorderDividerColor);
        jsonObject.put(KEY_IS_CHAR_BACKGROUND_BORDER_DIVIDER_STROKE_WIDTH,charBackgroundBorderDividerStrokeWidth);
        jsonObject.put(KEY_TIME_TEXT_SUBSCRIPT_FONT_SCALE,subscriptFontScale);
        jsonObject.put(KEY_TIME_TEXT_PADDING,timeTextPadding);
        jsonObject.put(KEY_IS_CHAR_BACKGROUND_BORDER_DUBBLE,charBackgroundBorderWithDoubble);
        jsonObject.put(KEY_TIME_TEXT_BASELINE_Y, baseLineX);
    }

    public static String integerArrayToString(Integer[] fromArrays){
        if(fromArrays==null)
            return "";
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<fromArrays.length;i++){
            if(i<fromArrays.length-1)
                sb.append(fromArrays[i]+",");
            else
                sb.append(fromArrays[i]);
        }
        return sb.toString();
    }

    public static Integer[] stringToIntegerArray(String fromString){
        if(fromString==null||fromString.isEmpty())
            return null;
        String[] temp=fromString.split(",");
        Integer[] ret=new Integer[temp.length];
        for(int i=0;i<temp.length;i++)
            ret[i]=Integer.valueOf(temp[i]);
        return ret;
    }

    public boolean isDisplaySecond() {
        return displaySecond;
    }

    public void setDisplaySecond(boolean displaySecond) {
        this.displaySecond = displaySecond;
    }

    public Integer[] getTimeColorsArray() {
        return timeColorsArray;
    }

    public void setTimeColorsArray(Integer[] timeColorsArray) {
        this.timeColorsArray = timeColorsArray;
    }

    public boolean isLinearGradientAble() {
        return linearGradientAble;
    }

    public void setLinearGradientAble(boolean linearGradientAble) {
        this.linearGradientAble = linearGradientAble;
    }

    public boolean isReflectedAble() {
        return reflectedAble;
    }

    public void setReflectedAble(boolean reflectedAble) {
        this.reflectedAble = reflectedAble;
    }

    public Integer getShadowType() {
        return shadowType;
    }

    public void setShadowType(Integer shadowType) {
        this.shadowType = shadowType;
    }

    public Integer getTimeTextCharAnimatorType() {
        return timeTextCharAnimatorType;
    }

    public void setTimeTextCharAnimatorType(Integer timeTextCharAnimatorType) {
        this.timeTextCharAnimatorType = timeTextCharAnimatorType;
    }

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public boolean isTimeTextSecondSubscript() {
        return timeTextSecondSubscript;
    }

    public void setTimeTextSecondSubscript(boolean timeTextSecondSubscript) {
        this.timeTextSecondSubscript = timeTextSecondSubscript;
    }

    public boolean isCharBackgroundBorder() {
        return charBackgroundBorder;
    }

    public void setCharBackgroundBorder(boolean charBackgroundBorder) {
        this.charBackgroundBorder = charBackgroundBorder;
    }

    public Integer getCharBackgroundBorderColor() {
        return charBackgroundBorderColor;
    }

    public void setCharBackgroundBorderColor(Integer charBackgroundBorderColor) {
        this.charBackgroundBorderColor = charBackgroundBorderColor;
    }

    public Integer getBaseLineDown() {
        return baseLineDown;
    }

    public void setBaseLineDown(Integer baseLineDown) {
        this.baseLineDown = baseLineDown;
    }

    public Integer getCharBackgroundBorderDividerColor() {
        return charBackgroundBorderDividerColor;
    }

    public void setCharBackgroundBorderDividerColor(Integer charBackgroundBorderDividerColor) {
        this.charBackgroundBorderDividerColor = charBackgroundBorderDividerColor;
    }

    public Integer getCharBackgroundBorderDividerStrokeWidth() {
        return charBackgroundBorderDividerStrokeWidth;
    }

    public void setCharBackgroundBorderDividerStrokeWidth(Integer charBackgroundBorderDividerStrokeWidth) {
        this.charBackgroundBorderDividerStrokeWidth = charBackgroundBorderDividerStrokeWidth;
    }

    public Integer getSubscriptFontScale() {
        return subscriptFontScale;
    }

    public void setSubscriptFontScale(Integer subscriptFontScale) {
        this.subscriptFontScale = subscriptFontScale;
    }

    public Integer getTimeTextPadding() {
        return timeTextPadding;
    }

    public void setTimeTextPadding(Integer timeTextPadding) {
        this.timeTextPadding = timeTextPadding;
    }

    public boolean isCharBackgroundBorderWithDoubble() {
        return charBackgroundBorderWithDoubble;
    }

    public void setCharBackgroundBorderWithDoubble(boolean charBackgroundBorderWithDoubble) {
        this.charBackgroundBorderWithDoubble = charBackgroundBorderWithDoubble;
    }

    public Integer getFontStyleSize() {
        return fontStyleSize;
    }

    public void setFontStyleSize(Integer fontStyleSize) {
        this.fontStyleSize = fontStyleSize;
    }

    public Integer getBaseLineX() {
        return baseLineX;
    }

    public void setBaseLineX(Integer baseLineX) {
        this.baseLineX = baseLineX;
    }
}
