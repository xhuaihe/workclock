package clock.socoolby.com.clock.fragment.handup;

import clock.socoolby.com.clock.state.ClockStateMachine;

public final class HandUpFragmentFactory {

    public static AbstractHandUpFragment build(String name, ClockStateMachine clockStateMachine){
        switch (name){
            case HandUpSelect123Fragment.NAME:
                return new HandUpSelect123Fragment();

        }
        return new HandUpDefaultFragment();
    }
}
