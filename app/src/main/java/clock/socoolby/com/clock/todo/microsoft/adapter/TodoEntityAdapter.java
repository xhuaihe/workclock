package clock.socoolby.com.clock.todo.microsoft.adapter;

import android.graphics.Paint;
import android.widget.ImageView;
import android.widget.TextView;

import com.idanatz.oneadapter.external.modules.ItemModule;
import com.idanatz.oneadapter.external.modules.ItemModuleConfig;
import com.idanatz.oneadapter.internal.holders.ViewBinder;

import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.todo.microsoft.bean.todo.TodoEntity;
import clock.socoolby.com.clock.todo.microsoft.bean.todo.TodoImportanceEnum;
import clock.socoolby.com.clock.todo.microsoft.bean.todo.TodoStatusEnum;
import clock.socoolby.com.clock.todo.microsoft.utils.TypeUtils;

public class TodoEntityAdapter extends ItemModule<TodoEntity> {

    @Override
    public void onBind(TodoEntity o, ViewBinder viewBinder) {

        TextView todo_time=viewBinder.findViewById(R.id.todo_tv_time);
        todo_time.setText(TypeUtils.timeFormat(o.getCreateddatetime()));

        TextView todo_name=viewBinder.findViewById(R.id.todo_tv_name);
        todo_name.setText(o.getSubject());

        if(o.getStatus()== TodoStatusEnum.completed){
            todo_name.getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG|Paint.ANTI_ALIAS_FLAG); // 设置中划线并加清晰
        }else{
            todo_name.getPaint().setFlags(0);
        }

        ImageView todo_pr=viewBinder.findViewById(R.id.todo_cb_importance);
        if(o.getImportance()== TodoImportanceEnum.HIGH)
           todo_pr.setImageResource(R.drawable.ic_star);
        else
           todo_pr.setImageResource(R.drawable.ic_star6);

        TextView todo_alter_time=viewBinder.findViewById(R.id.todo_tv_alter_time);
        if(o.getReminderdatetime()!=null){
            todo_alter_time.setText(TypeUtils.dateFormat(o.getReminderdatetime().getDateTime()));
        }else
            todo_alter_time.setText("-/-");
    }

    @Override
    public ItemModuleConfig provideModuleConfig() {
        return new ItemModuleConfig() {
            @Override
            public int withLayoutResource() { return R.layout.entity_todo; }
        };
    }
}
