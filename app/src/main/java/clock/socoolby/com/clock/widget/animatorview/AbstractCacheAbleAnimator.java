package clock.socoolby.com.clock.widget.animatorview;

import clock.socoolby.com.clock.cache.I_TrashCache;

public abstract class AbstractCacheAbleAnimator<T extends I_AnimatorEntry> extends AbstractCacheDifferenceAnimator<T,T> {

    public AbstractCacheAbleAnimator(int entryQuantity) {
        super(entryQuantity);
    }

    public AbstractCacheAbleAnimator(int entryQuantity, I_TrashCache<T> trashCache) {
        super(entryQuantity, trashCache);
    }

    public  void reset() {
        if (isCacheAble())
            moveToTrashCache(list);
        super.reset();
    }
}
