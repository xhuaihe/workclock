package clock.socoolby.com.clock.todo.microsoft.listener;

import java.util.List;

import clock.socoolby.com.clock.net.base.I_ResponseState;
import clock.socoolby.com.clock.net.listener.StateAbleRequestListener;
import clock.socoolby.com.clock.todo.microsoft.bean.todo.TodoFolder;

public class TodoFolderListRequestListener extends AbstractMicrosoftEntityListRequestListener<TodoFolder> {
    public TodoFolderListRequestListener(StateAbleRequestListener<List<TodoFolder>, I_ResponseState> warpListener) {
        super(warpListener);
    }

    @Override
    public TodoFolder creatEntity() {
        return new TodoFolder();
    }
}
