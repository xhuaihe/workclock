package clock.socoolby.com.clock.cache;

import java.util.List;

public interface I_TrashCache<Q>{

     void moveToTrashCache(List<Q> objs);

     void moveToTrashCache(Q obj);

     Q revectForTrashCache();

     int getTrashCacheSize();

     void clear();
}
