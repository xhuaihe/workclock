package clock.socoolby.com.clock.fragment.spine;

import clock.socoolby.com.clock.widget.spirit.AbstractLibgdxSpineEffectView;
import clock.socoolby.com.clock.widget.spirit.LibgdxSpineEffectView;

public class SpineFactory {

    public static AbstractLibgdxSpineEffectView build(String name){
        AbstractLibgdxSpineEffectView ret;
        switch (name){
            case LibgdxSpineEffectView.NAME:
                ret= new LibgdxSpineEffectView();
                break;
            default:
                ret= new LibgdxSpineEffectView();
        }
        return ret;
    }
}
