package clock.socoolby.com.clock.todo.microsoft.bean.todo;

public enum TodoSensitivityEnum {
    NORMAL("normal"),PERSONAL("personal"),PRIVATE("private"),CONFIDENTIAL("confidential");

    String value;

    TodoSensitivityEnum(String value) {
        this.value=value;
    }

    public String getValue() {
        return value;
    }

    public static TodoSensitivityEnum build(String value){
        for(TodoSensitivityEnum sensitivityEnum:TodoSensitivityEnum.values())
            if(value.equalsIgnoreCase(sensitivityEnum.value))
                return sensitivityEnum;
        return null;
    }
}
