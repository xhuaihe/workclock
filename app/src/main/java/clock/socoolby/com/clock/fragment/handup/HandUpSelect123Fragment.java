package clock.socoolby.com.clock.fragment.handup;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.util.Random;

import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.event.ClockEvent;
import clock.socoolby.com.clock.event.ClockEventListener;
import clock.socoolby.com.clock.event.EventListenerHandle;
import clock.socoolby.com.clock.event.EventManger;
import clock.socoolby.com.clock.state.ClockStateMachine;

public class HandUpSelect123Fragment extends AbstractHandUpSelectFragment implements View.OnClickListener {
    public static final String TAG=HandUpSelect123Fragment.class.getName();

    public static final String NAME="alter_select_123";

    Button selectButton1,selectButton2,selectButton3;

    int rundomButton;

    int roundLoop=10;

    Random random=new Random();
    int foregroundColor;

    EventListenerHandle heartBeatListenerHandle;

    public HandUpSelect123Fragment() {
        super( R.layout.fragment_handup_select_123);
    }

    @Override
    void bindView(View rootView) {
        selectButton1=rootView.findViewById(R.id.tv_select_button1);
        selectButton1.setOnClickListener(this);
        selectButton2=rootView.findViewById(R.id.tv_select_button2);
        selectButton2.setOnClickListener(this);
        selectButton3=rootView.findViewById(R.id.tv_select_button3);
        selectButton3.setOnClickListener(this);
    }


    @Override
    void bindViewModel() {
        foregroundColor=globalViewModel.getForegroundColor().getValue();

        rundomButton=random.nextInt(3);
        switch (rundomButton) {
            case 0:
                selectButton1.setText("是我啦");
                break;
            case 1:
                selectButton2.setText("是我啦");
                break;
            case 2:
                selectButton3.setText("是我啦");
                break;
        }

        heartBeatListenerHandle=EventManger.addHeartbeatListener(new ClockEventListener<Boolean>() {
            @Override
            public void onEvent(ClockEvent<Boolean> event) {
                timber.log.Timber.d("onEvent: "+event.getEventType()+"\troundLoop:"+roundLoop);
                if(roundLoop==0) {
                    setButtonColor(rundomButton);
                    EventManger.removeClockEventListener(heartBeatListenerHandle);
                    heartBeatListenerHandle=null;
                }else
                    setButtonColor(random.nextInt(3));
                roundLoop--;
            }
        });
    }

    private void setButtonColor(int index){
        switch (index){
            case 0:
                selectButton1.setBackgroundColor(Color.RED);
                selectButton2.setBackgroundColor(foregroundColor);
                selectButton3.setBackgroundColor(foregroundColor);
                break;
            case 1:
                selectButton1.setBackgroundColor(foregroundColor);
                selectButton2.setBackgroundColor(Color.RED);
                selectButton3.setBackgroundColor(foregroundColor);
                break;
            case 2:
                selectButton1.setBackgroundColor(foregroundColor);
                selectButton2.setBackgroundColor(foregroundColor);
                selectButton3.setBackgroundColor(Color.RED);
                break;
        }
    }


    @Override
    public void onClick(View v) {
        int selectIndex=0;
         switch (v.getId()){
             case R.id.tv_select_button1:
                 selectIndex=0;
                 break;
             case R.id.tv_select_button2:
                 selectIndex=1;
                 break;
             case R.id.tv_select_button3:
                 selectIndex=2;
                 break;
         }
         if(selectIndex==rundomButton)
             selected();
         else
             Toast.makeText(getActivity(),"HAHA,选错啦,不是我。",Toast.LENGTH_SHORT).show();
    }
}
