package clock.socoolby.com.clock.widget.animatorview;

import java.util.List;
import clock.socoolby.com.clock.cache.I_TrashCache;
import clock.socoolby.com.clock.cache.MemoryTrashCache;

public abstract class AbstractCacheDifferenceAnimator<T extends I_AnimatorEntry,Q> extends AbstractAnimator<T>{

    I_TrashCache<Q> trashCache;
    boolean globalTrashCashe=false;


    public AbstractCacheDifferenceAnimator(int entryQuantity) {
        this(entryQuantity,new MemoryTrashCache());
    }


    public AbstractCacheDifferenceAnimator(int entryQuantity,I_TrashCache<Q> trashCache){
       super(entryQuantity);
        this.trashCache=trashCache;
        this.globalTrashCashe=true;
    }

    public void stop() {
        super.stop();
        if(trashCache!=null&&!globalTrashCashe)
            trashCache.clear();
    }

    public boolean isCacheAble(){
        return true;
    }

    public void moveToTrashCache(List<Q> objs) {
          trashCache.moveToTrashCache(objs);
    }

    public void moveToTrashCache(Q obj) {
         trashCache.moveToTrashCache(obj);
    }

    public Q revectForTrashCache() {
        return trashCache.revectForTrashCache();
    }

    public int getTrashCacheSize() {
        return trashCache.getTrashCacheSize();
    }
}
