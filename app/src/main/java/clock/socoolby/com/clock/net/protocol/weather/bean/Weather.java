package clock.socoolby.com.clock.net.protocol.weather.bean;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.net.base.I_JsonObject;

public class Weather implements I_JsonObject {
    public String date;
    public String weather;
    public String wind;
    public String temperature;
    public String dayPictureUrl;
    public String nightPictureUrl;

    public String toString(){
        return date+"\n"+
                "气像:"+weather+"\n"+
                "风向:"+wind+"\n"+
                "温度:"+temperature;
    }

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        try {
            date = jsonObject.getString("date");
            weather = jsonObject.getString("weather");
            wind = jsonObject.getString("wind");
            temperature = jsonObject.getString("temperature");
            nightPictureUrl=jsonObject.getString("nightPictureUrl");
            dayPictureUrl=jsonObject.getString("dayPictureUrl");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {

    }


}
