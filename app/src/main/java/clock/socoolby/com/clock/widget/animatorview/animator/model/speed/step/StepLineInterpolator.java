package clock.socoolby.com.clock.widget.animatorview.animator.model.speed.step;


import clock.socoolby.com.clock.widget.animatorview.animator.model.speed.AbstractLineInterpolator;

public class StepLineInterpolator extends AbstractLineInterpolator {

    protected Float step;

    protected int speedUpDownPercent=1;

    public StepLineInterpolator(Float base, Float step) {
        this(base, Float.MAX_VALUE,step);
    }

    public StepLineInterpolator(Float base, Float maxValue, Float step) {
        super(base, maxValue);
        this.step=step;
    }

    @Override
    public void init() {
        super.init();
        speedUpDownPercent=1;
    }

    @Override
    public void speedUp(int percent) {
        speedUpDownPercent=speedUpDownPercent+speedUpDownPercent*percent/100;
    }

    @Override
    public void speedDown(int percent) {
        speedUpDownPercent=speedUpDownPercent-speedUpDownPercent*percent/100;
    }

    @Override
    public void resetSpeed() {
        speedUpDownPercent=1;
    }

    @Override
    public Float calculate(float timeGo, float base, float length, float totalTime) {
        return currentValue+step*speedUpDownPercent;
    }
}
