package clock.socoolby.com.clock.widget.animatorview.animator;

import android.graphics.Canvas;
import android.graphics.Paint;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

//引用： https://blog.csdn.net/findhappy117/article/details/79491531

public class DotsLineAnimator extends AbstractAnimator<DotsLineAnimator.Circle> {

    public static final String NAME="DotsLine";

    public static final int SLEEP_TIME=7;

    public DotsLineAnimator(int entryQuantity) {
        super(entryQuantity);
    }

    public DotsLineAnimator() {
        super(60);
    }

    @Override
    public int delayTime() {
        return SLEEP_TIME;
    }


    Circle circle;

    @Override
    public void onDraw(Canvas canvas) {
        for(int i=0;i<list.size();i++){
            circle=list.get(i);
            circle.drawCircle(canvas,mPaint);
            for (int j = i + 1; j < list.size(); j++) {
                circle.drawLine(canvas, list.get(j),mPaint);
            }
        }
    }

    @Override
    public Circle createNewEntry() {
        randomColorIfAble();
        return new Circle(rand.nextInt(width), rand.nextInt(height), rand.nextInt(30),rand.nextInt(30) , rand.nextInt(60) ,color);
    }


    public class Circle implements I_AnimatorEntry {
        float x,y;
        float r;
        float _mx,_my;
        int color;
        //创建对象
        //以一个圆为对象
        //设置随机的 percent，y坐标，r半径，_mx，_my移动的距离
        //this.r是创建圆的半径，参数越大半径越大
        //this._mx,this._my是移动的距离，参数越大移动
       public Circle(float x,float y,float mx,float my,float r,int color) {
            this.x = x;
            this.y = y;
            this.r = rand.nextInt(10) ;
            this._mx = rand.nextInt(20) ;
            this._my = rand.nextInt(30) ;
            this.color=color;
        }

        //canvas 画圆和画直线
        //画圆就是正常的用canvas画一个圆
        //画直线是两个圆连线，为了避免直线过多，给圆圈距离设置了一个值，距离很远的圆圈，就不做连线处理
        public void drawCircle(Canvas ctx,Paint paint) {
            paint.setColor(color);
            ctx.drawCircle(x,y,r,paint);
        }

        public void drawLine(Canvas ctx, Circle _circle,Paint paint) {
            paint.setColor(color);
            float dx = this.x - _circle.x;
            float dy = this.y - _circle.y;
            double d = Math.sqrt(dx * dx + dy * dy);
            if (d < 200) {
                ctx.drawLine(x,y,_circle.x,_circle.y,paint);
            }
        }
        // 圆圈移动
        // 圆圈移动的距离必须在屏幕范围内

        float w,h;
        @Override
        public void move(int maxWidth, int maxHight) {
            float w=new Float(maxWidth);
            float h=new Float(maxHight);
            this._mx = (this.x < w && this.x > 0) ? this._mx : (-this._mx);
            this._my = (this.y < h && this.y > 0) ? this._my : (-this._my);
            this.x += this._mx / 2;
            this.y += this._my / 2;
        }

        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {
            drawCircle(canvas,mPaint);
        }

        @Override
        public void setAnimatorEntryColor(int color) {
            this.color=color;
        }
    }
}
