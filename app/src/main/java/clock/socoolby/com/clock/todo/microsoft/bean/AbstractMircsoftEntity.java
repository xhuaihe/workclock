package clock.socoolby.com.clock.todo.microsoft.bean;

import com.idanatz.oneadapter.external.interfaces.Diffable;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.net.base.I_JsonObject;

public abstract class AbstractMircsoftEntity implements I_JsonObject , Diffable {

    public static final String ID="id";
    protected  String id;

    public  void fromJson(JSONObject jsonObject) throws JSONException {
        id=jsonObject.getString(ID);
    }

    public void toJson(JSONObject jsonObject) throws JSONException {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean areContentTheSame(Object o) {
        AbstractMircsoftEntity other;
        if(o instanceof AbstractMircsoftEntity)
            other=(AbstractMircsoftEntity)o;
        else
            return false;
        return other.id.equals(id);
    }

    @Override
    public long getUniqueIdentifier() {
        return 0;
    }
}
