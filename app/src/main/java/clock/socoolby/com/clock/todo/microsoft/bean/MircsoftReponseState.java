package clock.socoolby.com.clock.todo.microsoft.bean;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.net.base.I_ResponseState;
import clock.socoolby.com.clock.todo.microsoft.MicrosoftTodoSyncServiceImpl;

public class MircsoftReponseState implements I_ResponseState {
    public static final String NEXT_LINK="@odata.nextLink";
    public static final String CONTEXT="@odata.context";
    String nextLink;
    String context;

    @Override
    public boolean hasNext() {
        return nextLink!=null;
    }

    @Override
    public String nextLinkUrl() {
        return nextLink;
    }

    @Override
    public String getServiceName() {
        return MicrosoftTodoSyncServiceImpl.NAME;
    }

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        nextLink=jsonObject.optString(NEXT_LINK,null);
        context=jsonObject.optString(CONTEXT,null);
    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {

    }
}
