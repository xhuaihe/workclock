package clock.socoolby.com.clock.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import clock.socoolby.com.clock.model.SharePerferenceModel;
import clock.socoolby.com.clock.state.ClockThemeUITypeEnum;

public class ThemeUIViewModel extends ViewModel {
    //Theme UI部分
    private MutableLiveData<String> description=new MutableLiveData<>();

    private MutableLiveData<ClockThemeUITypeEnum> clockUITypeEnum=new MutableLiveData<>();

    private MutableLiveData<String> weatherDescription=new MutableLiveData<>();

    private MutableLiveData<String> dayDescription=new MutableLiveData<>();

    private MutableLiveData<String> weekDescription=new MutableLiveData<>();

    private MutableLiveData<Boolean> hand_time_visable=new MutableLiveData<>();

    private MutableLiveData<String> themeName=new MutableLiveData<>();

    SharePerferenceModel model;

    public ThemeUIViewModel(SharePerferenceModel model) {
        this.model = model;
        loadFromModel();
    }

    public void loadFromModel(){
         description.setValue(model.getDescription());

         themeName.setValue(model.getThemeName());

         clockUITypeEnum.setValue(ClockThemeUITypeEnum.valueOf(model.getThemeUIType()));
    }


    public MutableLiveData<String> getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description.setValue(description);
    }

    public MutableLiveData<ClockThemeUITypeEnum> getClockUITypeEnum() {
        return clockUITypeEnum;
    }

    public MutableLiveData<String> getWeatherDescription() {
        return weatherDescription;
    }

    public MutableLiveData<String> getDayDescription() {
        return dayDescription;
    }

    public MutableLiveData<String> getWeekDescription() {
        return weekDescription;
    }

    public MutableLiveData<Boolean> getHand_time_visable() {
        return hand_time_visable;
    }

    public MutableLiveData<String> getThemeName() {
        return themeName;
    }


    public void setClockUITypeEnum(ClockThemeUITypeEnum clockUITypeEnum) {
        this.clockUITypeEnum.setValue(clockUITypeEnum);
        model.setThemeUIType(clockUITypeEnum.code);
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription.setValue(weatherDescription);
    }

    public void setDayDescription(String dayDescription) {
        this.dayDescription.setValue(dayDescription);
    }

    public void setWeekDescription(String weekDescription) {
        this.weekDescription.setValue(weekDescription);
    }

    public void setHand_time_visable(Boolean hand_time_visable) {
        this.hand_time_visable.setValue(hand_time_visable);
    }

    public void setThemeName(String themeName) {
        this.themeName.setValue(themeName);
        model.setThemeName(themeName);
    }
}
