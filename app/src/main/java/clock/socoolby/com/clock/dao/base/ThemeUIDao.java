package clock.socoolby.com.clock.dao.base;

import e.odbo.data.dao.BaseDAO;
import e.odbo.data.dao.table.TableDefinition;
import e.odbo.data.dao.table.Field;

import com.openbravo.data.basic.BasicException;
import com.openbravo.data.loader.I_Session;
import com.openbravo.data.loader.serialize.Datas;
import e.odbo.data.format.Formats;
import com.openbravo.data.loader.serialize.DataWrite;
import com.openbravo.data.loader.serialize.DataRead;


public class ThemeUIDao extends BaseDAO<ThemeUI> {

    public ThemeUIDao(I_Session s) {
        super(s);
    }

    @Override
    public Class getSuportClass() {
        return ThemeUI.class;
    }

    // tableDefinition table:	theme_ui 	build time :2019-06-19 13:56:58.345

    @Override
    public TableDefinition getTable() {
        TableDefinition theme_uiTableDefinition=new TableDefinition("theme_ui" ,new Field[]{
                new Field(ThemeUI.ID,Datas.STRING,Formats.STRING),//(PK)
                new Field(ThemeUI.NAME,Datas.STRING,Formats.STRING),//(notNull)
                new Field(ThemeUI.CLOCK_TYPE,Datas.INT,Formats.INT),//(notNull)
                new Field(ThemeUI.CONFIG_TEXT,Datas.STRING,Formats.STRING)//(notNull)
        }, new int[] {0}
        );
        return theme_uiTableDefinition;
    }

    @Override
    public void writeInsertValues(DataWrite dp,ThemeUI obj) throws BasicException {
        dp.setString(1,obj.getId());
        dp.setString(2,obj.getName());
        dp.setInteger(3,obj.getClockType());
        dp.setString(4,obj.getConfigText());
    }


    public ThemeUI readValues(DataRead dr,ThemeUI obj) throws BasicException {
        if(obj==null)
            obj = new ThemeUI();
        obj.setId(dr.getString(1));
        obj.setName(dr.getString(2));
        obj.setClockType(dr.getInteger(3));
        obj.setConfigText(dr.getString(4));
        return obj;
    }
}