package clock.socoolby.com.clock.todo.microsoft.bean.base;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.net.base.I_JsonObject;
import clock.socoolby.com.clock.utils.JsonUtils;

/**
 *
 * https://docs.microsoft.com/zh-cn/graph/api/resources/recurrencepattern?view=graph-rest-beta
 *
 * dayOfMonth	Int32	事件在相应月份的多少号发生。 如果 type 为 absoluteMonthly 或 absoluteYearly，此为必需属性。
 * daysOfWeek	String 集合	事件在星期几（一系列值）发生。 可取值为：sunday、monday、tuesday、wednesday、thursday、friday 或 saturday。
 * 如果 type 为 relativeMonthly 或 relativeYearly，且 daysOfWeek 指定超过一天，事件遵循相应模式的第一天规则。
 * 如果 type 为 weekly、relativeMonthly 或 relativeYearly，此为必需属性。
 * firstDayOfWeek	字符串	每周的第一天。 可取值为：sunday、monday、tuesday、wednesday、thursday、friday 或 saturday。 默认值为 sunday。 如果 type 为 weekly，此为必需属性。
 * index	String	指定事件在 daysOfsWeek 中指定的第几个星期几实例发生，从相应月份的第一个实例开始计算。 可取值为：first、second、third、fourth 或 last。 默认值为 first。 如果 type 为 relativeMonthly 或 relativeYearly，请使用此可选属性。
 * interval	Int32	间隔的单元数，可以是天数、周数、月数或年数，具体视 type 而定。 必需。
 * month	Int32	事件发生的月份。 这是一个介于 1 到 12 之间的数字。
 * type	String	定期模式类型：daily、weekly、absoluteMonthly、relativeMonthly、absoluteYearly 或 relativeYearly。 此为必需属性。
 */

public class RecurrencePattern implements I_JsonObject {
      Integer  dayOfMonth;
      String[] daysOfWeek;
      String   firstDayOfWeek;
      String   index;
      Integer  interval;
      Integer  month;
      RecurrencePatternTypeEnum  type;

      @Override
      public void fromJson(JSONObject jsonObject) throws JSONException {
            type =RecurrencePatternTypeEnum.valueOf(jsonObject.getString("type"));
            switch (type){
                  case daily:
                        break;
                  case weekly:
                        firstDayOfWeek=jsonObject.optString("firstDayOfWeek","sunday");
                        daysOfWeek= JsonUtils.readStringArray(jsonObject,"daysOfWeek");
                        break;
                  case absoluteYearly:
                  case absoluteMonthly:
                        dayOfMonth=jsonObject.getInt("dayOfMonth");
                        break;
                  case relativeYearly:
                  case relativeMonthly:
                        index=jsonObject.optString("index","first");
                        daysOfWeek= JsonUtils.readStringArray(jsonObject,"daysOfWeek");
            }

            interval=jsonObject.getInt("interval");
            month=jsonObject.getInt("month");
      }

      @Override
      public void toJson(JSONObject jsonObject) throws JSONException {

      }
}
