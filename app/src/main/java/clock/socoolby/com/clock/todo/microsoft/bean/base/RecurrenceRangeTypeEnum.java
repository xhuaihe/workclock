package clock.socoolby.com.clock.todo.microsoft.bean.base;

/**
 *  type 属性	定期范围类型	说明	示例	必需属性
 *   endDate	有结束日期的范围	事件在 startDate 到 endDate（含边界值）之间遵循相应的定期模式重复发生。	事件在日期范围 2017 年 6 月 1 日到 2017 年 6 月 15 日之间重复发生。	type、startDate、endDate
 *   noEnd	无结束日期的范围	事件从 startDate 起遵循相应的定期模式重复发生。	事件在从 2017 年 6 月 1 日起的无限期日期范围内重复发生。	type、startDate
 *   numbered	指定了特定发生次数的范围	从 startDate 起，事件遵循定期模式重复发生 numberOfOccurrences 次。	事件在从 2017 年 6 月 1 日起的日期范围内重复发生 10 次。	type、startDate、numberOfOccurrences
 */

public enum RecurrenceRangeTypeEnum {
    endDate,noEnd,numbered
}
