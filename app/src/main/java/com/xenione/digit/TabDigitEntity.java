package com.xenione.digit;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

public class TabDigitEntity {

        private String currentChar,beforChar,nextChar;

        private final Matrix mModelViewMatrix = new Matrix();

        private final Matrix mModelViewProjectionMatrix = new Matrix();

        private final Matrix mRotationModelViewMatrix = new Matrix();

        private final RectF mStartBounds = new RectF();

        private final RectF mEndBounds = new RectF();

        private int mAlpha;

        private Matrix mMeasuredMatrixHeight = new Matrix();

        private Matrix mMeasuredMatrixWidth = new Matrix();

        private Matrix mProjectionMatrix;

        private int mCornerSize;

        private boolean charDrawCenter;

        private int startX;


    public TabDigitEntity(String currentChar, String nextChar,Matrix mProjectionMatrix,int mCornerSize,boolean charDrawCenter) {
        this.currentChar = currentChar;
        this.beforChar=currentChar;
        this.nextChar = nextChar;
        this.mProjectionMatrix=mProjectionMatrix;
        this.mCornerSize=mCornerSize;
        this.charDrawCenter=charDrawCenter;
    }

    public void measure(int width, int height) {
            Rect area = new Rect(-width / 2, 0, width / 2, height / 2);
            mStartBounds.set(area);
            mEndBounds.set(area);
            mEndBounds.offset(0, -height/2);
        }

        public int maxWith() {
            RectF rect = new RectF(mStartBounds);
            Matrix projectionMatrix = new Matrix();
            MatrixHelper.translate(projectionMatrix, mStartBounds.left, -mStartBounds.top, 0);
            mMeasuredMatrixWidth.reset();
            mMeasuredMatrixWidth.setConcat(projectionMatrix, MatrixHelper.ROTATE_X_90);
            mMeasuredMatrixWidth.mapRect(rect);
            return (int) rect.width();
        }

        public int maxHeight() {
            RectF rect = new RectF(mStartBounds);
            Matrix projectionMatrix = new Matrix();
            mMeasuredMatrixHeight.reset();
            mMeasuredMatrixHeight.setConcat(projectionMatrix, MatrixHelper.ROTATE_X_0);
            mMeasuredMatrixHeight.mapRect(rect);
            return (int) rect.height();
        }

        public void setChar(String currChar) {
            this.currentChar=currChar;
        }

        public void next() {
            if(currentChar!=nextChar)
                currentChar=nextChar;
            else
                currentChar=beforChar;
        }

        public void rotate(int alpha) {
            mAlpha = alpha;
            MatrixHelper.rotateX(mRotationModelViewMatrix, alpha);
        }

        public void draw(Canvas canvas, Rect mTextMeasured,Paint mNumberPaint,Paint mBackgroundPaint,int dividerStrokeWidth) {
            if(mBackgroundPaint!=null)
                drawBackground(canvas,mBackgroundPaint,dividerStrokeWidth);
            drawText(canvas, mTextMeasured, mNumberPaint);
        }

        private void drawBackground(Canvas canvas,Paint mBackgroundPaint,int dividerStrokeWidth) {
            canvas.save();
            mModelViewMatrix.set(mRotationModelViewMatrix);
            applyTransformation(canvas, mModelViewMatrix);
            canvas.drawRoundRect(mStartBounds, mCornerSize, mCornerSize, mBackgroundPaint);
            canvas.restore();
        }

        private void drawText(Canvas canvas, Rect mTextMeasured,Paint mNumberPaint) {
            canvas.save();
            mModelViewMatrix.set(mRotationModelViewMatrix);
            RectF clip = mStartBounds;
            if (mAlpha > 90) {
                mModelViewMatrix.setConcat(mModelViewMatrix, MatrixHelper.MIRROR_X);
                clip = mEndBounds;
            }
            applyTransformation(canvas, mModelViewMatrix);
            canvas.clipRect(clip);
            startX=-mTextMeasured.centerX();
            if(charDrawCenter)
                startX+=(mTextMeasured.width()-getCharWidth(currentChar,mNumberPaint))/2;
            canvas.drawText(currentChar, 0, 1,startX , -mTextMeasured.centerY(), mNumberPaint);
            canvas.restore();
        }

        private void applyTransformation(Canvas canvas, Matrix matrix) {
            mModelViewProjectionMatrix.reset();
            mModelViewProjectionMatrix.setConcat(mProjectionMatrix, matrix);
            canvas.concat(mModelViewProjectionMatrix);
        }


        public static int getCharWidth(String str, Paint paint) {
            return (int) paint.measureText(str);
        }
    }
